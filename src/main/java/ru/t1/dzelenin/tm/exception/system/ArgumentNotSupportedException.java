package ru.t1.dzelenin.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! This argument is not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Command ''" + argument + "'' is not supported...");
    }

}
